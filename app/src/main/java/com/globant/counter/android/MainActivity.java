package com.globant.counter.android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.globant.counter.android.mvp.model.CountModel;
import com.globant.counter.android.mvp.presenter.CountPresenter;
import com.globant.counter.android.mvp.view.CountView;

public class MainActivity extends AppCompatActivity {

    private CountPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new CountPresenter(new CountModel(), new CountView(this));
        presenter.subscribeBusEvents();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onClearBus();
    }

}
