package com.globant.counter.android.utils;

public abstract class CountBusObserver extends BusObserver<Integer> {
    public CountBusObserver() {
        super(Integer.class);
    }
}
