package com.globant.counter.android.mvp.presenter;

import android.app.Activity;

import com.globant.counter.android.mvp.model.CountModel;
import com.globant.counter.android.mvp.view.CountView;
import com.globant.counter.android.utils.CountBusObserver;
import com.globant.counter.android.utils.ResetBusObserver;
import com.globant.counter.android.utils.RxBus;

public class CountPresenter {

    private CountModel model;
    private CountView view;

    public CountPresenter(CountModel model, CountView view) {
        this.model = model;
        this.view = view;
    }


    public void subscribeBusEvents() {
        final Activity activity = view.getActivity();
        if (activity == null) {
            return;
        }
        RxBus.subscribe(activity, new CountBusObserver() {
            @Override
            public void onEvent(Integer value) {
                onCountButtonPressed();
            }
        });
        RxBus.subscribe(activity, new ResetBusObserver() {
            @Override
            public void onEvent(Boolean value) {
                onResetButtonPressed();
            }
        });
    }

    public void onCountButtonPressed() {
        model.inc();
        view.setCount(String.valueOf(model.getCount()));
    }

    public void onResetButtonPressed() {
        model.reset();
        view.setCount(String.valueOf(model.getCount()));
    }

    public void onClearBus() {
        final Activity activity = view.getActivity();
        if (activity == null) {
            return;
        }
        RxBus.clear(activity);
    }

}
