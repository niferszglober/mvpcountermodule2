package com.globant.counter.android.utils;

public abstract class ResetBusObserver extends BusObserver<Boolean> {
    public ResetBusObserver() {
        super(Boolean.class);
    }
}
